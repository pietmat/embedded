/*
 * week14_UART.c
 *
 * Created: 01/04/2020 16:18:20
 * Author : mateu
 */ 
#define F_CPU 8000000
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "communication.h"
#include "setup.h"

int main(void)
{
    setRegister();
	uart_interrupt();
	DDRB |= 0x01;
    while (1) 
    {
    }
}

ISR (USART_RX_vect)
{
	PORTB ^= 0x01;
	DataCom(UDR0);
}