/*
 * uart.c
 *
 * Created: 01/04/2020 16:30:49
 *  Author: mateu
 */ 
#include <avr/io.h>

void UART_Send(unsigned char send)
{
	while(!(UCSR0A & (1<<UDRE0)));
	UDR0 = send;
}

unsigned char UART_recieve(void)
{
	while(!(UCSR0A & (1<<RXC0)));
	return UDR0;
}