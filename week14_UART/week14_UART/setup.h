/*
 * setup.h
 *
 * Created: 01/04/2020 16:29:10
 *  Author: mateu
 */ 


#ifndef SETUP_H_
#define SETUP_H_

void uart_init(void);
void setRegister(void);
void uart_interrupt(void);



#endif /* SETUP_H_ */