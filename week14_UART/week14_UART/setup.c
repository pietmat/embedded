/*
 * setup.c
 *
 * Created: 01/04/2020 16:55:42
 *  Author: mateu
 */ 
#include <avr/io.h>
#include <avr/interrupt.h>

#define  BAUD 9600

#define  BAUDRATE 103

void uart_init(void)
{
	UBRR0H = (BAUDRATE >> 8);
	UBRR0L = BAUDRATE;
	//control register B side. 196
	UCSR0B |= (1<<TXEN0) | (1<RXEN0);
	//control register c side
	UCSR0C |= (3<<UCSZ00);
}

void setRegister(void)
{
	DDRB = 0x20; //edit here
}

void uart_interrupt(void)
{
	cli();
	
	UBRR0H = (BAUDRATE >> 8);
	UBRR0L = BAUDRATE;
	
	UCSR0B |= (1<<RXCIE0) | (1<<RXCIE0);
	
	UCSR0B |= (1<<TXEN0);
	
	UCSR0C |= (3<<UCSZ00);
	sei();
	 
}