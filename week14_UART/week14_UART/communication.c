/*
 * communication.c
 *
 * Created: 01/04/2020 16:40:25
 *  Author: mateu
 */ 

#include "uart.h"
#include <avr/io.h>

void DataCom (volatile unsigned char receive_data)
{
	if ((receive_data == '3'))
	{
		PORTB |=0x20;
		UART_Send('e');
		UART_Send('\r');
	}
	else if ((receive_data == '4'))
	{
		UART_Send('mateusz');
		UART_Send('\r');
		PORTB &= ~0x20;
	}
	else 
	{
		if (receive_data != '\r')
		{
			UART_Send('E');
			UART_Send('\r');
		}
	}
}
